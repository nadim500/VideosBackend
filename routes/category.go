package routes

import(
  "github.com/gorilla/mux"
  "../controllers"
)

func SetCategoryRoutes(router *mux.Router) *mux.Router{
  router.HandleFunc("/category",controllers.CreateCategory).Methods("POST")
  router.HandleFunc("/category",controllers.GetCategories).Methods("GET")
  return router
}
