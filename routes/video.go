package routes

import(
  "github.com/gorilla/mux"
  "../controllers"
)

func SetVideoRoutes(router *mux.Router) *mux.Router{
  router.HandleFunc("/video",controllers.CreateVideo).Methods("POST")
  router.HandleFunc("/video",controllers.GetVideos).Methods("GET")
  return router
}
