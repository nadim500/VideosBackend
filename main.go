package main

import(
  "log"
  "net/http"
  "./routes"
  "./common"
)

func main(){
  common.StartUp()
  router := routes.InitRoutes()
  server := &http.Server{
    Addr : common.AppConfig.Server,
    Handler : router,
  }
  log.Println("Listening...")
  server.ListenAndServe()
  defer common.GetSession().Close()
}
