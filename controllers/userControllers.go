package controllers

import(
  "log"
  "encoding/json"
  "net/http"
  "../data"
  "../common"
)

func Register(w http.ResponseWriter, r *http.Request){
  w.Header().Set("Access-Control-Allow-Origin","*")
  var dataResource UserResource
  err := json.NewDecoder(r.Body).Decode(&dataResource)
  if err != nil{
    log.Printf("[Register User Decode]: %s\n", err)
    common.DisplayError(
      w,
      err,
      "Invalid User Data",
      500,
    )
    return
  }
  user := &dataResource.Data
  err = data.CreateUser(user)
  if err != nil{
    common.DisplayError(
      w,
      err,
      "Error in create User",
      500,
    )
    return
  }
  user.HashPassword = nil
  j, err := json.Marshal(UserResource{Data: *user})
	if err != nil {
		log.Printf("[Error Marshal Register User]: %s\n", err)
		common.DisplayError(
      w,
      err,
      "Error convert Json",
      500,
    )
    return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(j)
}

func GetUsers(w http.ResponseWriter, r *http.Request){
  w.Header().Set("Access-Control-Allow-Origin","*")
  users, err := data.GetUsers()
  if err != nil{
    log.Printf("[Error Get Users]: %s\n")
    common.DisplayError(
      w,
      err,
      "Error in get users",
      500,
    )
    return
  }
  j, err := json.Marshal(UsersResource{Data: users})
  if err != nil{
    log.Printf("[Error Marshal Get Users] %s\n")
    common.DisplayError(
      w,
      err,
      "Error convert response to JSON",
      500,
    )
    return
  }
  w.Header().Set("Content-Type", "application/json")
  w.WriteHeader(http.StatusOK)
  w.Write(j)
}
