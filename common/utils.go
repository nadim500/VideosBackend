package common

import(
  "encoding/json"
  "log"
  "os"
  "time"
  "strings"
  //"strconv"
  "net/http"
)

type(

  appError struct{
    Error string `json:"error"`
    Message string `json:"message"`
    HttpStatus int `json:"status"`
  }

  errorResource struct{
    Data appError `json:"data"`
  }

  configuration struct{
    Server, MysqlDBHost, MysqlDBUser, MysqlDBPwd, Database string
  }
)

func DisplayError(w http.ResponseWriter, handlerError error, message string, code int){
  errObj:= appError{
    Error: handlerError.Error(),
    Message: message,
    HttpStatus: code,
  }
  w.Header().Set("Content-Type", "application/json; charset=utf-8")
  w.WriteHeader(code)
  if j,err := json.Marshal(errorResource{Data: errObj}); err == nil{
    w.Write(j)
  }

}

var AppConfig configuration

func initConfig(){
  loadAppConfig()
}

func loadAppConfig(){
  file, err := os.Open("./common/config.json")
  defer file.Close()
  if err != nil{
    log.Fatalf("[loadAppConfig load file]: %s\n", err)
    panic(err)
  }
  decoder := json.NewDecoder(file)
  AppConfig = configuration{}
  err = decoder.Decode(&AppConfig)
  if err != nil{
    log.Fatalf("[loadAppConfig decode file]: %s\n", err)
    panic(err)
  }
}

func NewId() int{
  hoy := time.Now()
  /*hora, min, sec := hoy.Clock()
  year, month, day := hoy.Date()
  nombre := strconv.Itoa(year) + strconv.Itoa(int(month))+ strconv.Itoa(day) + strconv.Itoa(hora) + strconv.Itoa(min) + strconv.Itoa(sec)+""
  res, err := strconv.Atoi(nombre)
  if err != nil{
    log.Fatalf("[Id error]: %s\n", err)
    panic(err)
  }
  return res*/
  var res int
  res = int(hoy.UnixNano())
  return res
}

func GetQuery(r *http.Request)(string, string){
  f := func(c rune) bool{
    return c =='[' || c == ']'
  }
  mapa := r.URL.Query()
  limit := r.URL.Query().Get("limit")
  order := r.URL.Query().Get("order")
  where := []string{}
  res_where := ""
  res_before_where := ""
  for key, value := range mapa{
    result := strings.FieldsFunc(key,f)
    if result[0] == "where"{
      clausule := ""
      for i:= range result{
        if i != 0{
          switch result[i]{
          case "eq":
            clausule += "="
          case "neq":
            clausule += "<>"
          case "lt":
            clausule += "<"
          case "gt":
            clausule += ">"
          case "lte":
            clausule += "<="
          case "gte":
            clausule += ">="
          default:
            clausule += "T1."+result[i]
          }
        }
      }
      clausule += value[0]
      where = append(where,clausule)
    }
  }
  if len(where) > 0{
    res_where = strings.Join(where," AND ")
    res_where = "WHERE " + res_where
  }
  if len(order)>0 || len(limit)>0{
    if len(order)>0{
      res_before_where += " ORDER BY " + order
    }
    if len(limit)>0{
      res_before_where += " LIMIT "+ limit
    }
    log.Println("RES_WHERE : ", res_where)
  }
  return res_where, res_before_where
}
