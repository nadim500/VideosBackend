package data

import(
  "../common"
  "../models"
  "log"
)

type VideoCategoryRepository struct{
  Insert string
}

var videoCategoryRepository = VideoCategoryRepository{
  Insert: "INSERT INTO video_category(videoid,categoryid) VALUES",
}

func CreateVideoCategory(categories []models.Category, videoId int) error{
  query := videoCategoryRepository.Insert
  vals := []interface{}{}
  for _, row := range categories {
    query += "(?,?),"
    vals = append(vals,videoId,row.Id)
  }
  query = query[0:len(query)-1]
  db := common.GetSession()
  stmt, err := db.Prepare(query)
  if err != nil{
    log.Printf("[Prepare query create video-category]: %s\n",err)
    return err
  }
  _,err = stmt.Exec(vals...)
  if err != nil{
    log.Printf("[Exec video-category create err]: %s\n",err)
    return err
  }
  return err
}
