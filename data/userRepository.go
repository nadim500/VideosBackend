package data

import(
  "golang.org/x/crypto/bcrypt"
  "../models"
  "../common"
  "log"
)

type UserRepository struct{
  Insert string
  GetAll string
}

var userRepository = UserRepository{
  Insert: "INSERT INTO users(id,username,email,password,datecreated) VALUES(?,?,?,?,?)",
  GetAll: "SELECT * FROM users",
}

func CreateUser(user *models.User) error{

  user.Id = common.NewId()
  hpass, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
  if err != nil{
    log.Printf("[hpass]: %s\n", err)
    return err
  }
  user.HashPassword = hpass
  user.Password = ""

  db := common.GetSession()
  stmt, err := db.Prepare(userRepository.Insert)
  if err != nil{
    log.Printf("[Prepare query error]: %s\n", err)
    return err
  }
  _, err = stmt.Exec(user.Id,user.Username,user.Email,user.HashPassword,user.DateCreated)
  if err != nil{
    log.Printf("[Exec query error]: %s\n", err)
    return err
  }
  return err
}

func GetUsers()([]models.User,error){
  users := make([]models.User,0)
  db := common.GetSession()
  stmt, err := db.Prepare(userRepository.GetAll)
  if err != nil{
    log.Printf("[Prepare query error GetAll User]: %s\n",err)
    return users, err
  }
  rows, err := stmt.Query()
  if err != nil{
    log.Printf("[Query error GetAll User]: %s\n",err)
    return users, err
  }
  defer rows.Close()
  result := models.User{}
  for rows.Next(){
    err = rows.Scan(&result.Id,&result.Username,&result.Email,&result.HashPassword,&result.DateCreated)
    result.HashPassword = nil
    users = append(users,result)
  }
  if err != nil{
    log.Printf("[Error Scan users]: %s\n",err)
    return users, err
  }
  return users, err
}
