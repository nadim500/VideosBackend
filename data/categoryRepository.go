package data

import(
  "../models"
  "../common"
  "log"
)

type CategoryRepository struct{
  Insert string
  GetAll string
}

var categoryRepository = CategoryRepository{
  Insert: "INSERT INTO categories(id,name) VALUES(?,?)",
  GetAll: "SELECT * FROM categories",
}

func CreateCategory(category *models.Category) error{
  category.Id = common.NewId()
  db := common.GetSession()
  stmt, err := db.Prepare(categoryRepository.Insert)
  if err!= nil{
    log.Printf("[Prepare query category create]: %s\n",err)
    return err
  }
  _, err = stmt.Exec(category.Id,category.Name)
  if err!= nil{
    log.Printf("[Exec category create err] %s\n",err)
    return err
  }
  return err
}

func GetCategories() ([]models.Category, error){
  categories := make([]models.Category, 0)
  db := common.GetSession()
  stmt, err := db.Prepare(categoryRepository.GetAll)
  if err !=nil{
    log.Printf("[Prepare query category get]: %s\n",err)
    return categories, err
  }
  rows, err := stmt.Query()
  if err != nil{
    log.Printf("[Query error category get]: %s\n",err)
    return categories, err
  }
  defer rows.Close()
  result := models.Category{}
  for rows.Next(){
    err = rows.Scan(&result.Id,&result.Name)
    categories = append(categories, result)
  }
  if err != nil{
    log.Printf("[Error Scan category]: %s\n",err)
    return categories, err
  }
  return categories, err
}
