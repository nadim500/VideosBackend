package routes

import(
  "github.com/gorilla/mux"
)

func InitRoutes() *mux.Router{
  router := mux.NewRouter().StrictSlash(false)
  router = SetUserRoutes(router)
  router = SetCategoryRoutes(router)
  router = SetVideoRoutes(router)
  return router
}
