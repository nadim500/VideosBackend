package data

import(
  "../common"
  "log"
)

type ImageRepository struct{
  Insert string
}

var imageRepository = ImageRepository{
  Insert: "INSERT INTO images(id,videoid,url) VALUES",
}

func CreateImage(images []string, videoId int) error{
  query := imageRepository.Insert
  vals := []interface{}{}
  for i, row := range images {
    query += "(?,?,?),"
    vals = append(vals,common.NewId()+i,videoId,row)
  }
  query = query[0:len(query)-1]
  db := common.GetSession()
  stmt, err := db.Prepare(query)
  if err != nil{
    log.Printf("[Prepare query create image]: %s\n",err)
    return err
  }
  _,err = stmt.Exec(vals...)
  if err != nil{
    log.Printf("[Exec image create err]: %s\n",err)
    return err
  }
  return err
}
