package data

import(
  "../common"
  "log"
)

type PlayRepository struct{
  Insert string
}

var playRepository = PlayRepository{
  Insert: "INSERT INTO play(id,videoid,url) VALUES",
}

func CreatePlay(plays []string, videoId int) error{
  query := playRepository.Insert
  vals := []interface{}{}
  for i, row := range plays {
    query += "(?,?,?),"
    vals = append(vals,common.NewId()+i,videoId,row)
  }
  query = query[0:len(query)-1]
  db := common.GetSession()
  stmt, err := db.Prepare(query)
  if err != nil{
    log.Printf("[Prepare query create play]: %s\n",err)
    return err
  }
  _,err = stmt.Exec(vals...)
  if err != nil{
    log.Printf("[Exec play create err]: %s\n",err)
    return err
  }
  return err
}
