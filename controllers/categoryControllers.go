package controllers

import(
  "log"
  "encoding/json"
  "net/http"
  "../data"
  "../common"
)

func CreateCategory(w http.ResponseWriter, r *http.Request){
  w.Header().Set("Access-Control-Allow-Origin","*")
  var dataResource CategoryResource
  err := json.NewDecoder(r.Body).Decode(&dataResource)
  if err != nil{
    log.Printf("[Category Data Decode]: %s\n", err)
    common.DisplayError(
      w,
      err,
      "Invalid Category Data",
      500,
    )
    return
  }
  category := &dataResource.Data
  err = data.CreateCategory(category)
  if err != nil{
    log.Printf("[Error Create Category]: %s\n",err)
    common.DisplayError(
      w,
      err,
      "Error create category",
      500,
    )
    return
  }
  j, err := json.Marshal(CategoryResource{Data: *category})
  if err!= nil{
    log.Printf("[Error Marshar Create Category] %s\n",err)
    common.DisplayError(
      w,
      err,
      "Error convert response to JSON",
      500,
    )
    return
  }
  w.Header().Set("Content-Type", "application/json")
  w.WriteHeader(http.StatusCreated)
  w.Write(j)
}

func GetCategories(w http.ResponseWriter, r *http.Request){
  w.Header().Set("Access-Control-Allow-Origin","*")
  categories, err := data.GetCategories()
  if err != nil{
    log.Printf("[Error Get Categories]: %s\n",err)
    common.DisplayError(
      w,
      err,
      "Error in get categories",
      500,
    )
    return
  }
  j, err := json.Marshal(CategoriesResource{Data: categories})
  if err != nil{
    log.Printf("[Error Marshall Get Categories] %s\n",err)
    common.DisplayError(
      w,
      err,
      "Error convert response to JSON",
      500,
    )
    return
  }
  w.Header().Set("Content-Type", "application/json")
  w.WriteHeader(http.StatusOK)
  w.Write(j)
}
