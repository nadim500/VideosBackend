package models

import(
)

type(

  User struct{
    Id int `json:"id"`
    Username string `json:"username"`
    Email string `json:"email"`
    Password string `json:"password,omitempty"`
    HashPassword []byte `json:"hashpassword,omitempty"`
    DateCreated string `json:"datecreated"`
  }

  Video struct{
    Id int `json:"id"`
    UserId int `json:"userid"`
    Name string `json:"name"`
    Description string `json:"description"`
    DateUpload string `json:"dateupload"`
    Visibility int `json:"visibility"`
    Likes int `json:"likes"`
    Dislikes int `json:"dislikes"`
    Views int `json:"views"`
    UrlVideos []string `json:"urlvideos"`
    UrlImages []string `json:"urlimages"`
    Categories []Category `json:"categories"`
  }

  Images struct{
    Id int `json:"id"`
    VideoId int `json:"videoid"`
    Url string `json:"url"`
  }

  Play struct{
    Id int `json:"id"`
    VideoId int `json:"videoid"`
    Url string `json:"url"`
  }

  Comment struct{
    UserId int `json:"userid"`
    VideoId int `json:"videoid"`
    Comment string `json:"comment"`
    DateCreated string `json:"datecreated"`
  }

  Category struct{
    Id int `json:"id"`
    Name string `json:"name"`
  }

  VideoCategory struct{
    VideoId int `json:"videoid"`
    CategoryId int `json:"categoryid"`
  }

)
