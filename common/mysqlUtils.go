package common

import(
  "log"
  _ "github.com/go-sql-driver/mysql"
  "database/sql"
)

var db *sql.DB

func GetSession() *sql.DB{
  if db == nil{
    log.Fatalf("[No exsite connexion db]: %s\n")
  }
  return db
}

func createDbSession(){
  var err error
  connection := GetConnection()
  db, err = sql.Open("mysql",connection)
  if err != nil{
    log.Fatalf("[Connect db]: %s\n", err)
    panic(err)
  }
}

func GetConnection() string{
  return ""+ AppConfig.MysqlDBUser+":"+AppConfig.MysqlDBPwd+"@/"+AppConfig.Database;
}
