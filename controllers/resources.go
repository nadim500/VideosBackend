package controllers

import(
  "../models"
)

type(

  UserResource struct{
    Data models.User `json:"data"`
  }

  UsersResource struct{
    Data []models.User `json:"data"`
  }

  CategoryResource struct{
    Data models.Category `json:"data"`
  }

  CategoriesResource struct{
    Data []models.Category `json:"data"`
  }

  VideoResource struct{
    Data models.Video `json:"data"`
  }

  VideosResource struct{
    Data []models.Video `json:"data"`
  }

  LoginResource struct{
    Data LoginModel `json:"data"`
  }

  LoginModel struct {
    Email string `json:"email"`
    Password string `json:"password"`
		Username string `json:"username"`
    DateCreated string `json:"datecreated"`
  }

)
