package data

import(
  "../models"
  "../common"
  "strconv"
  "database/sql"
  "strings"
  "log"
)

type VideoRepository struct{
  Insert string
  GetAll string
  GetLimit string
}

var videoRepository = VideoRepository{
  Insert: "INSERT INTO videos(id,userid,name,description,dateupload,visibility,likes,dislikes,views) VALUES(?,?,?,?,?,?,?,?,?)",
  //GetAll: "SELECT * FROM videos",
  GetAll: "SELECT T1.*, GROUP_CONCAT(distinct T2.url order by T2.url) AS urlvideos, GROUP_CONCAT(distinct T3.url order by T3.url) AS urlImages, GROUP_CONCAT(distinct T5.id) AS idcategory,GROUP_CONCAT(distinct T5.name) AS nameCategory FROM videos T1 LEFT JOIN play T2 on T1.id = T2.videoid LEFT JOIN images T3 on T1.id = T3.videoid LEFT JOIN video_category T4 on T1.id = T4.videoid LEFT JOIN categories T5 on T4.categoryid = T5.id GROUP BY T1.id",
  GetLimit: "SELECT T1.*, GROUP_CONCAT(distinct T2.url order by T2.url) AS urlvideos, GROUP_CONCAT(distinct T3.url order by T3.url) AS urlImages FROM videos T1 LEFT JOIN play T2 on T1.id = T2.videoid LEFT JOIN images T3 on T1.id = T3.videoid ",
}

func CreateVideo(video *models.Video) error{
  video.Id = common.NewId()
  video.Visibility = 0
  video.Likes = 0
  video.Dislikes = 0
  video.Views = 0
  db := common.GetSession()
  stmt, err := db.Prepare(videoRepository.Insert)
  if err != nil{
    log.Printf("[Prepare query create video]: %s\n",err)
    return err
  }
  _, err = stmt.Exec(video.Id,video.UserId,video.Name,video.Description,video.DateUpload,video.Visibility,video.Likes,video.Dislikes,video.Views)
  if err != nil{
    log.Printf("[Exec video create err]: %s\n",err)
    return err
  }
  if len(video.UrlImages) > 0{
    err = CreateImage(video.UrlImages, video.Id)
    if err != nil{
      log.Printf("[Create image error]: %s\n",err)
      return err
    }
  }
  if len(video.UrlVideos) > 0{
    err = CreatePlay(video.UrlVideos, video.Id)
    if err != nil{
      log.Printf("[Create play error]: %s\n",err)
      return err
    }
  }
  err = CreateVideoCategory(video.Categories, video.Id)
  if err != nil{
    log.Printf("[Create video-category error]: %s\n",err)
    return err
  }
  return err
}

func GetVideos() ([]models.Video, error){
  videos := make([]models.Video,0)
  db := common.GetSession()
  stmt, err := db.Prepare(videoRepository.GetAll)
  if err != nil{
    log.Printf("[Prepare query get videos]: %s\n",err)
    return videos, err
  }
  rows, err := stmt.Query()
  if err != nil{
    log.Printf("[Query error video get]: %s\n",err)
    return videos, err
  }
  defer rows.Close()
  result := models.Video{}
  urlVideos := sql.NullString{}
  urlImages := sql.NullString{}
  idCategory := sql.NullString{}
  nameCategory := sql.NullString{}
  for rows.Next(){
    err = rows.Scan(&result.Id,&result.UserId,&result.Name,&result.Description,&result.DateUpload,&result.Visibility,&result.Likes,&result.Dislikes,&result.Views, &urlVideos, &urlImages, &idCategory, &nameCategory)
    if urlVideos.Valid{
      result.UrlVideos = strings.Split(urlVideos.String,",")
    }else{
      result.UrlVideos = make([]string,0)
    }
    if urlImages.Valid{
      result.UrlImages = strings.Split(urlImages.String,",")
    }else{
      result.UrlImages = make([]string,0)
    }
    if idCategory.Valid && nameCategory.Valid{
      result.Categories = make([]models.Category,0)
      ids := strings.Split(idCategory.String,",")
      names := strings.Split(nameCategory.String,",")
      for i := range ids{
        idNumero,err := strconv.Atoi(ids[i])
        if err != nil{
          log.Printf("[Error convert numero id]: %s\n",err)
          return videos, err
        }
        nuevo := models.Category{
          Id: idNumero,
          Name: names[i],
        }
        result.Categories = append(result.Categories,nuevo)
      }
    }else{
      result.Categories = make([]models.Category,0)
    }
    videos = append(videos, result)
  }
  if err != nil{
    log.Printf("[Error Scan video]: %s\n",err)
    return videos, err
  }
  return videos, err
}

func GetVideosFilter(where string, order string)([]models.Video,error){
  videos := make([]models.Video,0)
  db := common.GetSession()
  query := where + " GROUP BY T1.id"+ order
  stmt, err := db.Prepare(videoRepository.GetLimit + query)
  if err != nil{
    log.Printf("[Prepare query get videos customize]: %s\n",err)
    return videos, err
  }
  rows, err := stmt.Query()
  if err != nil{
    log.Printf("[Query error video get customize]: %s\n",err)
    return videos, err
  }
  defer rows.Close()
  result := models.Video{}
  urlVideos := sql.NullString{}
  urlImages := sql.NullString{}
  for rows.Next(){
    err = rows.Scan(&result.Id,&result.UserId,&result.Name,&result.Description,&result.DateUpload,&result.Visibility,&result.Likes,&result.Dislikes,&result.Views, &urlVideos, &urlImages)
    if urlVideos.Valid{
      result.UrlVideos = strings.Split(urlVideos.String,",")
    }else{
      result.UrlVideos = make([]string,0)
    }
    if urlImages.Valid{
      result.UrlImages = strings.Split(urlImages.String,",")
    }else{
      result.UrlImages = make([]string,0)
    }
    videos = append(videos, result)
  }
  if err != nil{
    log.Printf("[Error Scan video customize]: %s\n",err)
    return videos, err
  }
  return videos, err
}
