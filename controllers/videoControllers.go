package controllers

import(
  "log"
  "encoding/json"
  "net/http"
  "../data"
  "../common"
  "../models"
)

func CreateVideo(w http.ResponseWriter, r *http.Request){
  w.Header().Set("Access-Control-Allow-Origin","*")
  var dataResource VideoResource
  err := json.NewDecoder(r.Body).Decode(&dataResource)
  if err != nil{
    log.Printf("[Video Data Decode]: %s\n", err)
    common.DisplayError(
      w,
      err,
      "Invalid Video Data",
      500,
    )
    return
  }
  video := &dataResource.Data
  err = data.CreateVideo(video)
  if err != nil{
    log.Printf("[Error Create Video]: %s\n",err)
    common.DisplayError(
      w,
      err,
      "Error create video",
      500,
    )
    return
  }
  j,err := json.Marshal(VideoResource{Data: *video})
  if err != nil{
    log.Printf("[Error Marshal create video]: %s\n",err)
    common.DisplayError(
      w,
      err,
      "Error convert response to json",
      500,
    )
    return
  }
  w.Header().Set("Content-Type", "application/json")
  w.WriteHeader(http.StatusCreated)
  w.Write(j)
}

func GetVideos(w http.ResponseWriter, r *http.Request){
  w.Header().Set("Access-Control-Allow-Origin","*")
  //videos, err := data.GetVideos()
  where, order := common.GetQuery(r)
  var videos []models.Video
  var err error
  log.Printf("[where] : %s [order]: %s ",where,order)
  if where == "" && order == ""{
    videos, err = data.GetVideos()
  }else{
    videos, err = data.GetVideosFilter(where,order)
  }
  if err != nil{
    log.Printf("[Error Get Videos]: %s\n",err)
    common.DisplayError(
      w,
      err,
      "Error in get Videos",
      500,
    )
    return
  }
  j, err := json.Marshal(VideosResource{Data: videos})
  if err != nil{
    log.Printf("[Error marshal get videos]: %s\n",err)
    common.DisplayError(
      w,
      err,
      "error convert to json",
      500,
    )
    return
  }
  w.Header().Set("Content-Type", "application/json")
  w.WriteHeader(http.StatusOK)
  w.Write(j)
}
